package com.example.robin.androidproject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class PledgeActivity extends Activity {

    ArrayList<Player> players  = new  ArrayList<Player>();

    ArrayList<String> namePlayer =  new ArrayList<String>();

    DbHelper dbHelper;
    int interratorLevel= 0 ;
    int interrator =0;
    ArrayList<LevelManager> levelManagers = new ArrayList<>();

    TextView tv_pledge ;
    TextView tv_level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setBackgroundColor(Color.argb(255,46,204,113));

        setContentView(R.layout.activity_pledge);

        namePlayer =  getIntent().getExtras().getStringArrayList("Players");
        tv_pledge=  (TextView) findViewById(R.id.textView_pledge);
        tv_level = (TextView) findViewById(R.id.textView_level);
        tv_level.setText("Level 1 ");
        setPlayer();

        dbHelper = new DbHelper(this);
        levelManagers.add(new LevelManager(1,players,dbHelper));
        levelManagers.add(new LevelManager(2,players,dbHelper));
        levelManagers.add(new LevelManager(3,players,dbHelper));
        levelManagers.add(new LevelManager(4,players,dbHelper));

        displayPledge();

    }

    private void setPlayer()
    {
        for (String tmp : namePlayer)
        {
            Player player_tmp  =new Player(tmp);
            player_tmp.setId(players.size());

            players.add(player_tmp);

        }
    }




    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    private void displayPledge()
    {
        tv_pledge.setText(levelManagers.get(interratorLevel).getPledges().get(interrator).getText());
    }

    public  void pledgeNext (View view)
    {
        changePledge();
    }

    public  void onClickDone (View view)
    {
        switch (levelManagers.get(interratorLevel).getPledges().get(interrator).getMode())
        {
            case Solo:
                Player playerToUp = levelManagers.get(interratorLevel).getPledges().get(interrator).getPlayer();
                playerToUp.AddGulp(levelManagers.get(interratorLevel).getPledges().get(interrator).getNbGulp());
                break;
        }

        changePledge();

    }

    private  void changePledge()
    {
        if(interrator+1 <levelManagers.get(interratorLevel).getPledges().size())
        {
            interrator ++;
        }
        else if( interratorLevel+1 == levelManagers.size())
        {
            Collections.sort(players);

            ArrayList<String> infosPlayers = new ArrayList<>();

            for (Player player: players)
            {
                infosPlayers.add(player.toString());
            }

            Intent intent = new Intent(getApplicationContext(), FinalActivity.class);

            intent.putExtra("Players_infos", infosPlayers);

            startActivity(intent);

        }
        else
        {
            /*
            /////////////////////////////////////////////////////////////
            Mettre ici Les Mini JEUX 

            ///////////////////////////////////////////////////////////////
             */
            interrator =0;
            interratorLevel ++;
            tv_level.setText("Level " + (interratorLevel+1) );
            switch (interratorLevel+1)
            {
                case 2:

                    getWindow().getDecorView().setBackgroundColor(Color.argb(255,52,152, 219 ));
                    break;
                case 3:
                    getWindow().getDecorView().setBackgroundColor(Color.argb(255,155,89,182));
                    break;
                case 4:
                    getWindow().getDecorView().setBackgroundColor(Color.argb(255,231,76,60));
                    break;
            }

        }

        displayPledge();
    }
}
