package com.example.robin.androidproject;

import android.support.annotation.NonNull;

/**
 * Created by Robin on 19/01/2018.
 */

public class Player implements Comparable
{
    private String Name ;
    private int Id;
    private int Gulp;
    private int Score;



    public Player (String _name )
    {
        Name = _name;
    }

    public Player() {

    }

    public void AddGulp(int input)
    {
        Gulp += input;
    }

    public int getGulp() {
        return Gulp;
    }

    public String getName() {
        return Name;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getId() {
        return Id;
    }

    public void setGulp(int gulp) {
        Gulp = gulp;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setScore(int score) {
        Score = score;
    }

    public int getScore() {
        return Score;
    }

    public String toString(){
        return  Name + "\t Gorgée: " + Gulp;
    }

    @Override
    public int compareTo(@NonNull Object playerToCompare)
    {
        int compareGulp=((Player)playerToCompare).getGulp();
        return compareGulp -  Gulp;
    }
}
