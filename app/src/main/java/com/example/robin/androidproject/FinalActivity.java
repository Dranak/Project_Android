package com.example.robin.androidproject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class FinalActivity extends Activity {

    ArrayList<String> namePlayer =  new ArrayList<String>();

    ArrayAdapter<String> adapter;

    ListView listView_player ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setBackgroundColor(Color.argb(255,231,76,60));

        setContentView(R.layout.activity_final);


        namePlayer =  getIntent().getExtras().getStringArrayList("Players_infos");

        listView_player = (ListView) findViewById(R.id.listView_winner);

        adapter = new ArrayAdapter<String>(this,R.layout.item_list_final,R.id.textView_Title,namePlayer);

        listView_player.setAdapter(adapter);

    }

    public void GoBack(View view)
    {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

        startActivity(intent);
    }
}
