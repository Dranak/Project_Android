package com.example.robin.androidproject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Robin on 20/01/2018.
 */

public class Pledge {

    Integer id;
    String title;
    String text;
    Player player;
    Integer level;
    Integer nbGulp;
    Mode mode;


    public Pledge ()
    {

    }

    public Pledge (String _text,String _title, Integer _level)
    {
        text = _text;
        level= _level;
        title =_title;
    }

    public Pledge (String _text, Integer _level, Integer _nbGulp, Mode _mode)
    {
        text = _text;
        level =_level;
        nbGulp = _nbGulp;
        mode = _mode;
    }



    public String toString(){
        return "Text:" + text + " Level:" + level + " Nb Gulp:" + nbGulp + " Mode:" + mode ;
    }

    public void setId(Integer _id) {
        this.id = _id;
    }

    public Integer getId() {
        return id;
    }


    public void setPlayer(Player player) {
        this.player = player;
        String[] text_tmp = text.split(" ");
        String text_tmp2 = "";
        for (String str: text_tmp)
        {
            if(str.equals("*"))
            {
                text_tmp2 += player.getName();
            }
            else
            {
                text_tmp2 += str;
            }
            text_tmp2 += " ";
        }

        text = text_tmp2;

    }

    public Player getPlayer() {
        return player;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle (String title) { this.title = title;}

    public String getTitle() {
        return title;
    }

    public void setLevel (Integer _level) { this.level = _level;}

    public Integer getLevel() {
        return level;
    }

    public void setNbGulp (Integer _nbgulp) { this.nbGulp = _nbgulp;}

    public Integer getNbGulp() {
        return nbGulp;
    }

    public void setMode(Mode _mode) {
        this.mode = _mode;
    }

    public Mode getMode() {
        return mode;
    }
}
