package com.example.robin.androidproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.SourceLocator;

/**
 * Created by roro on 28/01/18.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 16;
    private static final String DATABASE_NAME = "alcolo";
    private SQLiteDatabase dbase;
    private Context context;
    private InputStream inputStream;

    //Player Table
    public static final String TABLE_PLAYER = "player";
    private static final String KEY_PLAYER_ID = "id";
    private static final String KEY_PLAYER_NAME = "name";
    private static final String KEY_PLAYER_SCORE = "score";

    //Pledge Table
    public static final String TABLE_PLEDGE = "pledge";
    private static final String KEY_PLEDGE_ID = "id";
    private static final String KEY_PLEDGE_TEXT = "text";
    private static final String KEY_PLEDGE_LEVEL = "level";
    private static final String KEY_PLEDGE_NBGULP = "nb_gulp";
    private static final String KEY_PLEDGE_MODE = "mode";


    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        dbase = db;
        String sqlTablePledge = "CREATE TABLE IF NOT EXISTS " + TABLE_PLEDGE +
                "(" + KEY_PLEDGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_PLEDGE_TEXT + " TEXT, "
                + KEY_PLEDGE_LEVEL + " INTEGER, "
                + KEY_PLEDGE_NBGULP + " INTEGER, "
                + KEY_PLEDGE_MODE + " TEXT "
                +")";
        String sqlTablePlayer = "CREATE TABLE IF NOT EXISTS " + TABLE_PLAYER +
                "(" + KEY_PLAYER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_PLAYER_NAME + " TEXT, "
                + KEY_PLAYER_SCORE + " INTEGER "
                +")";
        db.execSQL(sqlTablePledge);
        db.execSQL(sqlTablePlayer);
        addPledgesFromCsv();
        addPlayers();
        /*db.close();*/
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLEDGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYER);
        onCreate(db);
    }

    //Pledges
    public void addPledgesFromCsv() {
        this.inputStream = context.getResources().openRawResource(R.raw.gages);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(";");

                if(row.length == 5){
                    String text = String.valueOf(row[0]);
                    Integer level = Integer.parseInt(String.valueOf(row[1]));
                    Integer nbGulp = Integer.parseInt(String.valueOf(row[2]));
                    Mode mode = Mode.valueOf(String.valueOf(row[4]));
                    Pledge p1 = new Pledge(text, level, nbGulp, mode);
                    this.addPledge(p1);
                }

            }
        } catch (IOException ex) {
            throw new RuntimeException("Problème de merde avec csv: " + ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new RuntimeException("Autre problème de merde: " + e);
            }
        }
    }

    private void addPledge(Pledge pledge){
        ContentValues values = new ContentValues();
        values.put(KEY_PLEDGE_TEXT, pledge.getText());
        values.put(KEY_PLEDGE_LEVEL, pledge.getLevel());
        values.put(KEY_PLEDGE_NBGULP, pledge.getNbGulp());
        values.put(KEY_PLEDGE_MODE, pledge.getMode().toString());
        dbase.insert(TABLE_PLEDGE, null, values);
    }

    public Pledge getPledgeById(int id){
        String selectQuery = "SELECT * FROM " + TABLE_PLEDGE + " WHERE id=" + id;
        dbase=this.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        Pledge pledge = null;
        try{
            if(cursor.moveToFirst()){
                String text = cursor.getString(1);
                Integer level =  cursor.getInt(2);
                Integer nbGulp = cursor.getInt(3);
                Mode mode = Mode.valueOf(cursor.getString(4));;
                pledge = new Pledge(text, level, nbGulp, mode);
            }
        } finally {
            cursor.close();
        }

        return pledge;
    }

    public ArrayList<Pledge> getPledgesByLevel(int level, int nb_pledges) {
        ArrayList<Pledge> pledgeList = new ArrayList<Pledge>();
        String selectQuery = "SELECT  * FROM " + TABLE_PLEDGE + " WHERE "+KEY_PLEDGE_MODE+" !=  \"Many\" AND level=" + level + " ORDER BY random() LIMIT " + nb_pledges;
        dbase=this.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Pledge pledge = new Pledge();
                pledge.setId(cursor.getInt(0));
                pledge.setText(cursor.getString(1));
                pledge.setLevel(cursor.getInt(2));
                pledge.setNbGulp(cursor.getInt(3));
                pledge.setMode(Mode.valueOf(cursor.getString(4)));
                pledgeList.add(pledge);
            } while (cursor.moveToNext());
        }
        return pledgeList; //return list of pledges by level
    }


    //Player
    private void addPlayers() {
        Player p1 = new Player("Player 1");
        this.addPlayer(p1);
        Player p2 = new Player("Player 2");
        this.addPlayer(p2);
        Player p3 = new Player("Player 3");
        this.addPlayer(p3);
    }

    private void addPlayer(Player player){
        ContentValues values = new ContentValues();
        values.put(KEY_PLAYER_NAME, player.getName());
        values.put(KEY_PLAYER_SCORE, 0);
        dbase.insert(TABLE_PLAYER, null, values);
    }

    public ArrayList<Player> getAllPlayers() {
        ArrayList<Player> playerList = new ArrayList<Player>();
        String selectQuery = "SELECT  * FROM " + TABLE_PLAYER;
        dbase=this.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Player player = new Player();
                player.setId(cursor.getInt(0));
                player.setName(cursor.getString(1));
                player.setScore(cursor.getInt(2));
                playerList.add(player);
            } while (cursor.moveToNext());
        }
        return playerList; //return list of player
    }

    public Player getPlayerById(int id){
        String selectQuery = "SELECT * FROM " + TABLE_PLAYER + " WHERE id=" + id;
        dbase=this.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
        Player player = null;
        try{
            if(cursor.moveToFirst()){
                String name = cursor.getString(1);
                player = new Player(name);
            }
        } finally {
            cursor.close();
        }

        return player;
    }


    //Utility
    public int rowcount()
    {
        int row=0;
        String selectQuery = "SELECT  * FROM " + TABLE_PLEDGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        row=cursor.getCount();
        return row;
    }


}
