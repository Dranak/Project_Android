package com.example.robin.androidproject;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Robin on 10/02/2018.
 */

public class LevelManager {

     private int level ;
    private  ArrayList<Player> players = new ArrayList<>();
    private ArrayList<Pledge> pledges = new ArrayList<>();

    public  DbHelper dbHelper ;

    public LevelManager (int _level, ArrayList<Player> _players,DbHelper _dbHelper )
    {
        dbHelper = _dbHelper;
        level = _level;
        pledges=  dbHelper.getPledgesByLevel(level,10);
        players = _players;


        InitAllPledges();
    }

    private void InitAllPledges ()
    {
        Random rand = new Random();
        for (Pledge pledge_tmp: pledges)
        {
            switch (pledge_tmp.getMode())
            {
                case Everybody:
                    if( pledge_tmp.getText().contains("*"))
                    {
                        pledge_tmp.setPlayer(players.get(rand.nextInt(players.size())));
                    }

                    break;
                case Looser:
                    if( pledge_tmp.getText().contains("*"))
                    {
                        pledge_tmp.setPlayer(players.get(rand.nextInt(players.size())));
                    }
                    break;
                case Solo:
                        pledge_tmp.setPlayer(players.get(rand.nextInt(players.size())));
                    break;
                case Many:
                 /*   if( pledge_tmp.getText().contains("*"))
                    {
                        pledge_tmp.setPlayer(players.get(rand.nextInt(players.size())));
                    }
*/
                    break;
            }

        }

    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public ArrayList<Pledge> getPledges() {
        return pledges;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public void setPledges(ArrayList<Pledge> pledges) {
        this.pledges = pledges;
    }

}
